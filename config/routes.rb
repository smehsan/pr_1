Rails.application.routes.draw do
  root 'pages#home'
  get 'about', to: 'pages#about'
  get 'contact',to: 'pages#contact'
  get 'blog', to: 'posts#index'

  devise_for :users
  
  resources :users, only: [:index, :show]

  resources :posts, except: [:index]
end
