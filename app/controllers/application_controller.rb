class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_in, keys: [:user_name, :email])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:user_name, :email, :password, :password_confirmation])
    devise_parameter_sanitizer.permit(:account_update, keys: [:user_name, :email, :password, :password_confirmation, :current_password])
  end

  def user_allowed(post)
    unless current_user.id == post.user.id
      flash[:danger] = "Permission Denied, You are not the author of this post!"
      redirect_to post_path(@post)
    end
  end

end
