class PagesController < ApplicationController
  def home
    @posts = Post.limit(4).order('created_at desc')
  end

  def about
  end

  def contact
  end

  def blog
  end
end
