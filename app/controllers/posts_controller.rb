class PostsController < ApplicationController
  include Pagy::Backend
  before_action :post_find, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]
  before_action -> { user_allowed(@post) }, except: [:index, :show, :new, :create]
  
  def index
    @pagy, @posts = pagy(Post.all.order('created_at desc'), items: 3)
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    @post.user = current_user
    if @post.save
      flash[:success] = "Post created successfully."
      redirect_to post_path(@post)
    else
      render 'new'
    end
  end

  def show
  end

  def edit
    # if current_user.id != @post.user_id 
    #   flash[:danger] = "Yoou are not authorized to edit this post."
    #   redirect_to post_path(@post)
    # end
  end

  def update
    if @post.update(post_params)
      flash[:success] = "Post updated successfully"
      redirect_to post_path(@post)
    else
      render 'edit'
    end
  end

  def destroy
    @post.destroy
    flash[:danger] = "Post deleted successfully"
    redirect_to blog_path
  end


  private
  def post_find
    @post = Post.find(params[:id])
  end
  def post_params
    params.require(:post).permit(:title, :content, :image)
  end
end
