class Post < ApplicationRecord
  has_one_attached :image
  
  validates :title, 
            presence: true, 
            uniqueness: true, 
            length: {minimum: 3, maximum: 200}
  validates :content, 
            presence: true, 
            length: {minimum: 7, maximum: 3000}
  
  belongs_to :user
end
